#ifndef ROBOT_H
#define ROBOT_H

//////////////////////////
// Taylor Wilcox		//
//////////////////////////

#include "Application.h"

__declspec(align(16)) class Robot{

public:
	
	Robot(float fX = 0.0f, float fY = 0.0f, float fZ = 0.0f);
	~Robot();

	static void LoadResources(); 
	static void ReleaseResources();
	void Update();
	void Draw();

	void SetWorldPosition(float fX, float fY, float fZ);

private:

	void UpdateMatrices();

	static CommonMesh* s_pBody;
	static CommonMesh* s_pLeftAnkle;
	static CommonMesh* s_pLeftElbow;
	static CommonMesh* s_pLeftHip;
	static CommonMesh* s_pLeftKnee;
	static CommonMesh* s_pLeftShoulder;
	static CommonMesh* s_pLeftWrist;
	static CommonMesh* s_pNeck;
	static CommonMesh* s_pPelvis;
	static CommonMesh* s_pRightAnkle;
	static CommonMesh* s_pRightElbow;
	static CommonMesh* s_pRightHip;
	static CommonMesh* s_pRightKnee;
	static CommonMesh* s_pRightShoulder;
	static CommonMesh* s_pRightWrist;

	static bool s_bResourcesReady;

	XMFLOAT4 m_v4Pos;

	XMMATRIX m_mWorldMatrix;

	XMMATRIX m_mBody;
	XMMATRIX m_mLeftAnkle;
	XMMATRIX m_mLeftElbow;
	XMMATRIX m_mLeftHip;
	XMMATRIX m_mLeftKnee;
	XMMATRIX m_mLeftShoulder;
	XMMATRIX m_mLeftWrist;
	XMMATRIX m_mNeck;
	XMMATRIX m_mPelvis;
	XMMATRIX m_mRightAnkle;
	XMMATRIX m_mRightElbow;
	XMMATRIX m_mRightHip;
	XMMATRIX m_mRightKnee;
	XMMATRIX m_mRightShoulder;
	XMMATRIX m_mRightWrist;

	XMFLOAT4 m_v4BodyOff;
	XMFLOAT4 m_v4LeftAnkleOff;
	XMFLOAT4 m_v4LeftElbowOff;
	XMFLOAT4 m_v4LeftHipOff;
	XMFLOAT4 m_v4LeftKneeOff;
	XMFLOAT4 m_v4LeftShoulderOff;
	XMFLOAT4 m_v4LeftWristOff;
	XMFLOAT4 m_v4NeckOff;
	XMFLOAT4 m_v4PelvisOff;
	XMFLOAT4 m_v4RightAnkleOff;
	XMFLOAT4 m_v4RightElbowOff;
	XMFLOAT4 m_v4RightHipOff;
	XMFLOAT4 m_v4RightKneeOff;
	XMFLOAT4 m_v4RightShoulderOff;
	XMFLOAT4 m_v4RightWristOff;

	XMFLOAT4 m_v4CamRot;							// Local rotation angles
	XMFLOAT4 m_v4CamOff;							// Local offset

	XMVECTOR m_vCamWorldPos;						// World position
	XMMATRIX m_mCamWorldMatrix;						// Camera's world transformation matrix

public:

	float GetXPosition(void) { return m_v4Pos.x; }
	float GetYPosition(void) { return m_v4Pos.y; }
	float GetZPosition(void) { return m_v4Pos.z; }

	XMFLOAT4 GetFocusPosition(void) { return GetPosition(); }
	XMFLOAT4 GetCameraPosition(void) { XMFLOAT4 v4Pos; XMStoreFloat4(&v4Pos, m_vCamWorldPos); return v4Pos; }
	XMFLOAT4 GetPosition(void) { return m_v4Pos; }

	void* operator new(size_t i)
	{
		return _mm_malloc(i, 16);
	}

	void operator delete(void* p)
	{
		_mm_free(p);
	}
};

#endif