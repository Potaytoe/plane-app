#include "Robot.h"

//////////////////////////
// Taylor Wilcox		//
//////////////////////////

CommonMesh* Robot::s_pBody = NULL;
CommonMesh* Robot::s_pLeftAnkle = NULL;
CommonMesh* Robot::s_pLeftElbow = NULL;
CommonMesh* Robot::s_pLeftHip = NULL;
CommonMesh* Robot::s_pLeftKnee = NULL;
CommonMesh* Robot::s_pLeftShoulder = NULL;
CommonMesh* Robot::s_pLeftWrist = NULL;
CommonMesh* Robot::s_pNeck = NULL;
CommonMesh* Robot::s_pPelvis = NULL;
CommonMesh* Robot::s_pRightAnkle = NULL;
CommonMesh* Robot::s_pRightElbow = NULL;
CommonMesh* Robot::s_pRightHip = NULL;
CommonMesh* Robot::s_pRightKnee = NULL;
CommonMesh* Robot::s_pRightShoulder = NULL;
CommonMesh* Robot::s_pRightWrist = NULL;

bool Robot::s_bResourcesReady = false;

Robot::Robot(float fX, float fY, float fZ){

	m_mWorldMatrix = XMMatrixIdentity();

	m_mBody = XMMatrixIdentity();
	m_mLeftAnkle = XMMatrixIdentity();
	m_mLeftElbow = XMMatrixIdentity();
	m_mLeftHip = XMMatrixIdentity();
	m_mLeftKnee = XMMatrixIdentity();
	m_mLeftShoulder = XMMatrixIdentity();
	m_mLeftWrist = XMMatrixIdentity();
	m_mNeck = XMMatrixIdentity();
	m_mPelvis = XMMatrixIdentity();
	m_mRightAnkle = XMMatrixIdentity();
	m_mRightElbow = XMMatrixIdentity();
	m_mRightHip = XMMatrixIdentity();
	m_mRightKnee = XMMatrixIdentity();
	m_mRightShoulder = XMMatrixIdentity();
	m_mRightWrist = XMMatrixIdentity();

	m_v4Pos = XMFLOAT4(fX, fY, fZ, 0.0f);

	m_v4BodyOff = XMFLOAT4(0.500099, 43.749992, 0.000003, 0.0f);
	m_v4LeftAnkleOff = XMFLOAT4(-0.800152, -36.399994, -0.000098, 0.0f);
	m_v4LeftElbowOff = XMFLOAT4(34.250019, -0.499817, -0.004262, 0.0f);
	m_v4LeftHipOff = XMFLOAT4(19.500000, -7.724991, 0.000000, 0.0f);
	m_v4LeftKneeOff = XMFLOAT4(0.000006, -22.200001, 0.000000, 0.0f);
	m_v4LeftShoulderOff = XMFLOAT4(46.000000, 0.000000, -0.009992, 0.0f);
	m_v4LeftWristOff = XMFLOAT4(55.250008, -0.999710, 0.003968, 0.0f);
	m_v4NeckOff = XMFLOAT4(0.249983, 36.625015, 25.999998, 0.0f);
	m_v4PelvisOff = XMFLOAT4(-0.250011, 15.250000, -0.000005, 0.0f);
	m_v4RightAnkleOff = XMFLOAT4(0.199911, -36.799995, 0.000039, 0.0f);
	m_v4RightElbowOff = XMFLOAT4(-33.999996, 0.250229, -0.000194, 0.0f);
	m_v4RightHipOff = XMFLOAT4(-19.500000, -7.724991, 0.000000, 0.0f);
	m_v4RightKneeOff = XMFLOAT4(0.000006, -22.000000, 0.000000, 0.0f);
	m_v4RightShoulderOff = XMFLOAT4(-44.500023, 0.500000, -0.000021, 0.0f);
	m_v4RightWristOff = XMFLOAT4(-60.000381, -1.750183, 0.007156, 0.0f);

	m_vCamWorldPos = XMVectorZero();
}

Robot::~Robot()
{

}

void Robot::LoadResources()
{
	s_pBody = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/body.x");
	s_pLeftAnkle = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/left_ankle.x");
	s_pLeftElbow = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/left_elbow.x");
	s_pLeftHip = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/left_hip.x");
	s_pLeftKnee = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/left_knee.x");
	s_pLeftShoulder = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/left_shoulder.x");
	s_pLeftWrist = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/left_wrist.x");
	s_pNeck = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/neck.x");
	s_pPelvis = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/pelvis.x");
	s_pRightAnkle = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/right_ankle.x");
	s_pRightElbow = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/right_elbow.x");
	s_pRightHip = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/right_hip.x");
	s_pRightKnee = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/right_knee.x");
	s_pRightShoulder = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/right_shoulder.x");
	s_pRightWrist = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Robot/right_wrist.x");
}

void Robot::ReleaseResources()
{
	delete s_pBody;
	delete s_pLeftAnkle;
	delete s_pLeftElbow;
	delete s_pLeftHip;
	delete s_pLeftKnee;
	delete s_pLeftShoulder;
	delete s_pLeftWrist;
	delete s_pNeck;
	delete s_pPelvis;
	delete s_pRightAnkle;
	delete s_pRightElbow;
	delete s_pRightHip;
	delete s_pRightKnee;
	delete s_pRightShoulder;
	delete s_pRightWrist;
}

void Robot::Draw()
{
	Application::s_pApp->SetWorldMatrix(m_mBody);
	s_pBody->Draw();
	Application::s_pApp->SetWorldMatrix(m_mLeftAnkle);
	s_pLeftAnkle->Draw();
	Application::s_pApp->SetWorldMatrix(m_mLeftElbow);
	s_pLeftElbow->Draw();
	Application::s_pApp->SetWorldMatrix(m_mLeftHip);
	s_pLeftHip->Draw();
	Application::s_pApp->SetWorldMatrix(m_mLeftKnee);
	s_pLeftKnee->Draw();
	Application::s_pApp->SetWorldMatrix(m_mLeftShoulder);
	s_pLeftShoulder->Draw();
	Application::s_pApp->SetWorldMatrix(m_mLeftWrist);
	s_pLeftWrist->Draw();
	Application::s_pApp->SetWorldMatrix(m_mNeck);
	s_pNeck->Draw();
	Application::s_pApp->SetWorldMatrix(m_mPelvis);
	s_pPelvis->Draw();
	Application::s_pApp->SetWorldMatrix(m_mRightAnkle);
	s_pRightAnkle->Draw();
	Application::s_pApp->SetWorldMatrix(m_mRightElbow);
	s_pRightElbow->Draw();
	Application::s_pApp->SetWorldMatrix(m_mRightHip);
	s_pRightHip->Draw();
	Application::s_pApp->SetWorldMatrix(m_mRightKnee);
	s_pRightKnee->Draw();
	Application::s_pApp->SetWorldMatrix(m_mRightShoulder);
	s_pRightShoulder->Draw();
	Application::s_pApp->SetWorldMatrix(m_mRightWrist);
	s_pRightWrist->Draw();
}

void Robot::Update()
{
	UpdateMatrices();
}

void Robot::UpdateMatrices()
{
	XMMATRIX mTrans;
	mTrans = XMMatrixTranslation(GetXPosition(), GetYPosition(), GetZPosition());

	m_mWorldMatrix = mTrans;

	mTrans = XMMatrixTranslation(m_v4BodyOff.x / 10, m_v4BodyOff.y / 10, m_v4BodyOff.z / 10);

	m_mBody = mTrans * m_mWorldMatrix;
	
	mTrans = XMMatrixTranslation(m_v4NeckOff.x / 10, m_v4NeckOff.y / 10, m_v4NeckOff.z / 10);

	m_mNeck = mTrans * m_mBody;

	mTrans = XMMatrixTranslation(m_v4LeftShoulderOff.x / 10, m_v4LeftShoulderOff.y / 10, m_v4LeftShoulderOff.z / 10);

	m_mLeftShoulder = mTrans * m_mBody;

	mTrans = XMMatrixTranslation(m_v4LeftElbowOff.x / 10, m_v4LeftElbowOff.y / 10, m_v4LeftElbowOff.z / 10);

	m_mLeftElbow = mTrans * m_mLeftShoulder;

	mTrans = XMMatrixTranslation(m_v4LeftWristOff.x / 10, m_v4LeftWristOff.y / 10, m_v4LeftWristOff.z / 10);

	m_mLeftWrist = mTrans * m_mLeftShoulder;

	mTrans = XMMatrixTranslation(m_v4RightShoulderOff.x / 10, m_v4RightShoulderOff.y / 10, m_v4RightShoulderOff.z / 10);

	m_mRightShoulder = mTrans * m_mBody;

	mTrans = XMMatrixTranslation(m_v4RightElbowOff.x / 10, m_v4RightElbowOff.y / 10, m_v4RightElbowOff.z / 10);

	m_mRightElbow = mTrans * m_mRightShoulder;

	mTrans = XMMatrixTranslation(m_v4RightWristOff.x / 10, m_v4RightWristOff.y / 10, m_v4RightWristOff.z / 10);

	m_mRightWrist = mTrans * m_mRightShoulder;

	mTrans = XMMatrixTranslation(m_v4PelvisOff.x / 10, m_v4PelvisOff.y / 10, m_v4PelvisOff.z / 10);

	m_mPelvis = mTrans * m_mWorldMatrix;

	mTrans = XMMatrixTranslation(m_v4LeftHipOff.x / 10, m_v4LeftHipOff.y / 10, m_v4LeftHipOff.z / 10);

	m_mLeftHip = mTrans * m_mPelvis;

	mTrans = XMMatrixTranslation(m_v4LeftKneeOff.x / 10, m_v4LeftKneeOff.y / 10, m_v4LeftKneeOff.z / 10);

	m_mLeftKnee = mTrans * m_mLeftHip;

	mTrans = XMMatrixTranslation(m_v4LeftAnkleOff.x / 10, m_v4LeftAnkleOff.y / 10, m_v4LeftAnkleOff.z / 10);

	m_mLeftAnkle = mTrans * m_mLeftKnee;

	mTrans = XMMatrixTranslation(m_v4RightHipOff.x / 10, m_v4RightHipOff.y / 10, m_v4RightHipOff.z / 10);

	m_mRightHip = mTrans * m_mPelvis;

	mTrans = XMMatrixTranslation(m_v4RightKneeOff.x / 10, m_v4RightKneeOff.y / 10, m_v4RightKneeOff.z / 10);

	m_mRightKnee = mTrans * m_mRightHip;

	mTrans = XMMatrixTranslation(m_v4RightAnkleOff.x / 10, m_v4RightAnkleOff.y / 10, m_v4RightAnkleOff.z / 10);

	m_mRightAnkle = mTrans * m_mRightKnee;

	XMMATRIX mRotXC, mRotYC, mRotZC, mTransC;
	
	mTransC = XMMatrixTranslation(0.0f, 20.5f, -25.0f);

	mRotXC = XMMatrixRotationX(XMConvertToRadians(m_v4CamRot.x));
	mRotYC = XMMatrixRotationY(XMConvertToRadians(m_v4CamRot.y));
	mRotZC = XMMatrixRotationZ(XMConvertToRadians(m_v4CamRot.z));

	m_mCamWorldMatrix = mRotXC * mRotYC * mRotZC * mTransC;

	m_vCamWorldPos = m_mCamWorldMatrix.r[3];

}