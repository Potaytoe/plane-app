#ifndef BULLET_H
#define BULLET_H

//////////////////////////
// Taylor Wilcox		//
//////////////////////////

#include "Application.h"

class Bullet
{
public:
	
	enum { TOTAL_Bullets = 64 };
	
	Bullet();
	~Bullet();

	static CommonMesh*	s_pBulletMesh;

	static bool s_bResourcesReady;

	void Update(XMVECTOR fV);
	static void LoadResources(void);		// Only load the resources once for all instances 
	static void ReleaseResources(void);	// Only free the resources once for all instances 
	void Draw();

	void SetWorldPosition(float fX, float fY, float fZ);

	bool GetActive() const {
		return mActive;
	}
	void SetActive(bool active) {
		mActive = active;
	}

	float GetXPositionB(void) { return m_v4PosB.x; }
	float GetYPositionB(void) { return m_v4PosB.y; }
	float GetZPositionB(void) { return m_v4PosB.z; }
	XMFLOAT4 GetPosition(void) { return m_v4PosB; }

	void Fire(XMVECTOR fV, float fX, float fY, float fZ, float fZP);

	void SetPos(XMVECTOR fV, float fX, float fY, float fZ, float fZP) //set bullet position, forward vector and plane position
	{
		m_vForwardVector = fV;
		m_v4PosB = XMFLOAT4(fX - 0.1f, fY + 0.1f, fZ, 0.2f);
	}

	XMMATRIX getMatrix() { return m_mBulletMatrix; }

	void* operator new(size_t i)
	{
		return _mm_malloc(i, 16);
	}

		void operator delete(void* p)
	{
		_mm_free(p);
	}

private:
	void UpdateMatrices(void);

	XMFLOAT4 m_v4BulletOff;
	XMMATRIX m_mBulletMatrix;
	XMFLOAT4 m_v4BulletRot;
	XMFLOAT4 m_v4PosB;

	XMVECTOR m_vForwardVector;
	
	float m_fSpeed;
	bool mActive;

};

#endif