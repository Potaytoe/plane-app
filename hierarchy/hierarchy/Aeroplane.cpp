//*********************************************************************************************
// File:			Aeroplane.cpp
// Description:		A very simple class to represent an aeroplane as one object with all the 
//					hierarchical components stored internally within the class.	
//*********************************************************************************************
//////////////////////////
// Taylor Wilcox		//
//////////////////////////

#include "Aeroplane.h"

// Initialise static class variables.
CommonMesh* Aeroplane::s_pPlaneMesh = NULL;
CommonMesh* Aeroplane::s_pPropMesh = NULL;
CommonMesh* Aeroplane::s_pTurretMesh = NULL;
CommonMesh* Aeroplane::s_pGunMesh = NULL;


bool Aeroplane::s_bResourcesReady = false;

Aeroplane::Aeroplane( float fX, float fY, float fZ, float fRotY )
{
	m_mWorldMatrix = XMMatrixIdentity();
	m_mPropWorldMatrix = XMMatrixIdentity();
	m_mTurretWorldMatrix = XMMatrixIdentity();
	m_mGunWorldMatrix = XMMatrixIdentity();
	m_mCamWorldMatrix = XMMatrixIdentity();

	m_v4Rot = XMFLOAT4(0.0f, fRotY, 0.0f, 0.0f);
	m_v4Pos = XMFLOAT4(fX, fY, fZ, 0.0f);

	m_v4PropOff = XMFLOAT4(0.0f, 0.0f, 1.9f, 0.0f);
	m_v4PropRot = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);

	m_v4TurretOff = XMFLOAT4(0.0f, 1.05f, -1.3f, 0.0f);
	m_v4TurretRot = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);

	m_v4GunOff = XMFLOAT4(0.0f, 0.5f, 0.0f, 0.0f);
	m_v4GunRot = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);

	m_v4CamOff = XMFLOAT4(0.0f, 4.5f, -15.0f, 0.0f);
	m_v4CamRot = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);

	m_vCamWorldPos = XMVectorZero();
	m_vForwardVector = XMVectorZero();

	m_fSpeed = 0.0f;

	m_bGunCam = false;
	
	for (int i = 0; i < TOTAL_Bullets; ++i) 
		mBullets.push_back(new Bullet);
}

Aeroplane::~Aeroplane( void )
{
}

void Aeroplane::SetWorldPosition( float fX, float fY, float fZ  )
{
	m_v4Pos = XMFLOAT4(fX, fY, fZ, 0.0f);
	UpdateMatrices();
}


void Aeroplane::UpdateMatrices( void )
{
	XMMATRIX mRotX, mRotY, mRotZ, mTrans, mTransP, mTransT, mTransG, mTransC, mTransB, Moveage,
		mRotXP, mRotYP, mRotZP, mRotXT, mRotYT, mRotZT, mRotXG, mRotYG, mRotZG, mRotXB, mRotYB, mRotZB, mRotXC, mRotYC, mRotZC;
	XMMATRIX mPlaneCameraRot, mForwardMatrix, m_mIgnoreMatrix;
	
	// Calculate m_mWorldMatrix for plane based on Euler rotation angles and position data.

	mRotX = XMMatrixRotationX(XMConvertToRadians(m_v4Rot.x));
	mRotY = XMMatrixRotationY(XMConvertToRadians(m_v4Rot.y));
	mRotZ = XMMatrixRotationZ(XMConvertToRadians(m_v4Rot.z));

	mRotXP = XMMatrixRotationX(XMConvertToRadians(m_v4PropRot.x));
	mRotYP = XMMatrixRotationY(XMConvertToRadians(m_v4PropRot.y));
	mRotZP = XMMatrixRotationZ(XMConvertToRadians(m_v4PropRot.z));

	mRotXT = XMMatrixRotationX(XMConvertToRadians(m_v4TurretRot.x));
	mRotYT = XMMatrixRotationY(XMConvertToRadians(m_v4TurretRot.y));
	mRotZT = XMMatrixRotationZ(XMConvertToRadians(m_v4TurretRot.z));

	mRotXG = XMMatrixRotationX(XMConvertToRadians(m_v4GunRot.x));
	mRotYG = XMMatrixRotationY(XMConvertToRadians(m_v4GunRot.y));
	mRotZG = XMMatrixRotationZ(XMConvertToRadians(m_v4GunRot.z));

	mRotXC = XMMatrixRotationX(XMConvertToRadians(m_v4CamRot.x));
	mRotYC = XMMatrixRotationY(XMConvertToRadians(m_v4CamRot.y));
	mRotZC = XMMatrixRotationZ(XMConvertToRadians(m_v4CamRot.z));

	mTrans = XMMatrixTranslation(GetXPosition(), GetYPosition(), GetZPosition());
	mTransP = XMMatrixTranslation(0.0f, 0.0f, 1.9f);
	mTransT = XMMatrixTranslation(0.0f, 1.05f, -1.3f);
	mTransG = XMMatrixTranslation(0.0f, 0.5f, 0.0f);
	mTransC = XMMatrixTranslation(0.0f, 4.5f, -15.0f);
	
	m_mWorldMatrix = mRotX * mRotZ * mRotY * mTrans;

	// calculate a matrix which ignores rotations in Z and X for the camera to parent to

	m_mIgnoreMatrix = mRotY * mTrans;

	// Get the forward vector out of the world matrix

	m_vForwardVector = m_mWorldMatrix.r[2];

	// Calculate m_mPropWorldMatrix for propellor based on Euler rotation angles and position data.
	// Parent the propellor to the plane

	m_mPropWorldMatrix = mRotXP * mRotYP * mRotZP * mTransP;
	m_mPropWorldMatrix = m_mPropWorldMatrix * m_mWorldMatrix;

	// Calculate m_mTurretWorldMatrix for propellor based on Euler rotation angles and position data.
	// Parent the turret to the plane

	m_mTurretWorldMatrix = mRotXT * mRotYT * mRotZT * mTransT;
	m_mTurretWorldMatrix = m_mTurretWorldMatrix * m_mWorldMatrix;

	// Calculate m_mGunWorldMatrix for gun based on Euler rotation angles and position data.
	// Parent the gun to the turret

	m_mGunWorldMatrix = mRotXG * mRotYG * mRotZG * mTransG;
	m_mGunWorldMatrix = m_mGunWorldMatrix * m_mTurretWorldMatrix;

	// Calculate m_mCameraWorldMatrix for camera based on Euler rotation angles and position data.

	m_mCamWorldMatrix = mRotXC * mRotYC * mRotZC * mTransC * m_mIgnoreMatrix;

	// Switch between parenting the camera to the plane (without X and Z rotations) and the gun based on m_bGunCam
	
	if (m_bGunCam)
		m_mCamWorldMatrix = mRotX * mRotY * mRotZ * mTrans * m_mGunWorldMatrix;

	// Get the camera's world position out of the matrix

	m_vCamWorldPos = m_mCamWorldMatrix.r[3];

}

void Aeroplane::Update( bool bPlayerControl )
{
	
	// All of this section
	if( bPlayerControl )
	{
		// Step 1: Make the plane pitch upwards on the up arrow key and return to level when released
		// Maximum pitch = 60 degrees
		if (Application::s_pApp->IsKeyPressed('Q'))
		{
			if (m_v4Rot.x <= 60)
			{
				m_v4Rot.x++;
			}
		}
		else
		{
			if (m_v4Rot.x > 0)
			{
				m_v4Rot.x--;
			}
		}

		// Step 2: Make the plane pitch downwards on the down arrow key and return to level when released
		// You can also impose a take off seeepd of 0.5 if you like
		// Minimum pitch = -60 degrees
		if (Application::s_pApp->IsKeyPressed('A'))
		{
			if (m_v4Rot.x >= -60)
			{
				m_v4Rot.x--;
			}
		}
		else
		{
			if (m_v4Rot.x < 0)
			{
				m_v4Rot.x++;
			}
		}

		// Step 3: Make the plane yaw and roll left on the left arrow key and return to level when released
		// Maximum roll = 20 degrees
		if (Application::s_pApp->IsKeyPressed('O'))
		{
			m_v4Rot.y--;
			if (m_v4Rot.z <= 20)
			{	
				m_v4Rot.z++;
			}	
				
		}
		else
			if (m_v4Rot.z > 0)
				{
					m_v4Rot.z--;
				}

		// Step 4: Make the plane yaw and roll right on the right arrow key and return to level when released
		// Minimum roll = -20 degrees
		if (Application::s_pApp->IsKeyPressed('P'))
		{
			m_v4Rot.y++;
			if (m_v4Rot.z >= -20)
			{		
				m_v4Rot.z--;
			}
		}
		else
			if (m_v4Rot.z < 0)
			{
				m_v4Rot.z++;
			}

			if (Application::s_pApp->IsKeyPressed(VK_SPACE))
			{
				for (int i = 0; i < mBullets.size(); ++i)
				{
					Fire();

					XMVECTOR gCurrPos;
					gCurrPos = m_mGunWorldMatrix.r[3];
					XMStoreFloat4(&m_v4PosG, gCurrPos);

					XMVECTOR pCurrPos;
					pCurrPos = m_mPropWorldMatrix.r[3];
					XMStoreFloat4(&m_v4PosP, pCurrPos);

				}
			}
			

	} // End of if player control

	// Apply a forward thrust and limit to a maximum speed of 1
	m_fSpeed += 0.001f;

	if( m_fSpeed > 1 )
		m_fSpeed = 1;

	// Rotate propellor and turret
	m_v4PropRot.z += 100 * m_fSpeed;
	m_v4TurretRot.y += 0.1f;

	// Tilt gun up and down as turett rotates
	m_v4GunRot.x = (sin((float)XMConvertToRadians(m_v4TurretRot.y*4.0f)) * 10.0f) - 10.0f;

	UpdateMatrices();

	// Move Forward
	XMVECTOR vCurrPos = XMLoadFloat4(&m_v4Pos);
	vCurrPos += m_vForwardVector * m_fSpeed;
	XMStoreFloat4(&m_v4Pos, vCurrPos);

	for (int i = 0; i < mBullets.size(); ++i)
	{
		Bullet &b = *mBullets[i];
		b.Update(m_mGunWorldMatrix.r[2]);
	}
}

void Aeroplane::LoadResources( void )
{
	s_pPlaneMesh = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Plane/plane.x");
	s_pPropMesh = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Plane/prop.x");
	s_pTurretMesh = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Plane/turret.x");
	s_pGunMesh = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Plane/gun.x");
	Bullet::LoadResources();
}

void Aeroplane::ReleaseResources( void )
{
	delete s_pPlaneMesh;
	delete s_pPropMesh;
	delete s_pTurretMesh;
	delete s_pGunMesh;
	Bullet::ReleaseResources();
}

void Aeroplane::Draw( void )
{
	Application::s_pApp->SetWorldMatrix( m_mWorldMatrix );
	s_pPlaneMesh->Draw();

	Application::s_pApp->SetWorldMatrix( m_mPropWorldMatrix );
	s_pPropMesh->Draw();

	Application::s_pApp->SetWorldMatrix( m_mTurretWorldMatrix );
	s_pTurretMesh->Draw();

	Application::s_pApp->SetWorldMatrix( m_mGunWorldMatrix );
	s_pGunMesh->Draw();

	for (int i = 0; i < mBullets.size(); ++i)
	{
		Bullet &b = *mBullets[i];
		if (b.GetActive())
		{
			b.Draw();
		}
	}

}

void Aeroplane::Fire()
{
	for (int i = 0; i < mBullets.size(); i++)
	{
		Bullet &b = *mBullets[i];
		if (!b.GetActive())
		{
			XMVECTOR gCurrPos;
			gCurrPos = m_mGunWorldMatrix.r[3];
			XMStoreFloat4(&m_v4PosG, gCurrPos);
			mBullets[i]->SetPos(m_mGunWorldMatrix.r[2], m_v4PosG.x, m_v4PosG.y, m_v4PosG.z, m_v4PosP.z);
			mBullets[i]->SetActive(true);
		}
		
	}
}





