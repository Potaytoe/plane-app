#include "Bullet.h"

//////////////////////////
// Taylor Wilcox		//
//////////////////////////

// Initialise static class variables.
CommonMesh* Bullet::s_pBulletMesh = NULL;

bool Bullet::s_bResourcesReady = false;
Aeroplane* pAero = nullptr;

Bullet::Bullet()
{
	m_mBulletMatrix = XMMatrixIdentity();
	m_v4BulletOff = XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f);
	m_v4BulletRot = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	m_fSpeed = 1.2f;									// Bullet speed.
	mActive = false;									// No active bullets at start.
}

Bullet::~Bullet()
{

}

void Bullet::SetWorldPosition(float fX, float fY, float fZ)
{
	m_v4PosB = XMFLOAT4(fX, fY, fZ, 0.0f);
	UpdateMatrices();
}

void Bullet::UpdateMatrices()
{
	XMMATRIX mRotXB, mRotYB, mRotZB, mTransB;
	mRotXB = XMMatrixRotationX(XMConvertToRadians(m_v4BulletRot.x));
	mRotYB = XMMatrixRotationY(XMConvertToRadians(m_v4BulletRot.y));
	mRotZB = XMMatrixRotationZ(XMConvertToRadians(m_v4BulletRot.z));
	mTransB = XMMatrixTranslation(GetXPositionB(), GetYPositionB(), GetZPositionB());

	m_mBulletMatrix = mRotXB * mRotYB * mRotZB * mTransB;
}

void Bullet::Update(XMVECTOR fV)
{
	XMVECTOR vCurrPos = XMLoadFloat4(&m_v4PosB);
	vCurrPos += m_vForwardVector * m_fSpeed;
	XMStoreFloat4(&m_v4PosB, vCurrPos);

	UpdateMatrices();
}

void Bullet::LoadResources()
{
	s_pBulletMesh = CommonMesh::LoadFromXFile(Application::s_pApp, "Resources/Plane/bullet.x");
}

void Bullet::ReleaseResources()
{
	delete s_pBulletMesh;
}

void Bullet::Draw()
{
	//if (mActive){
		Application::s_pApp->SetWorldMatrix(m_mBulletMatrix);
		s_pBulletMesh->Draw();
	//}

}

void Bullet::Fire(XMVECTOR fV, float fX, float fY, float fZ, float fZP)
{
	//mActive = true;
	m_vForwardVector = fV;
	m_v4PosB = XMFLOAT4(fX-0.1f, fY+0.1f, fZ, 0.2f);
}